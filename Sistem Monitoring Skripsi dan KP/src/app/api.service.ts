import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
  	private http:HttpClient
  ) { }

  serverUrl:any='http://localhost/kp_sk/index.php/';
  
    login(data){
    return this.http.post(this.serverUrl+'Auth/login',data);
  }
  test_pendaftaran_kp(){
    return this.http.get(this.serverUrl+'pendaftaran_kp/status');
  }
  
  test_dashboard(){
    return this.http.get(this.serverUrl+'dashboard_kaprodi/status');
  }
  mahasiswa(data){
    return this.http.get(this.serverUrl+'dashboard_kaprodi/mahasiswa');
  }
  mhs()
  {
    return this.http.get(this.serverUrl+'Data_mhs');
  }
   get_pendaftaran_sk()
  {
    return this.http.get(this.serverUrl+'Pendaftaran_sk');
  }

  get_verifikasi()
  {
    return this.http.get(this.serverUrl+'Verifikasi_kp');
  }
  test_vr()
  {
    return this.http.get(this.serverUrl+'Verifikasi_kp/status');
  }
}