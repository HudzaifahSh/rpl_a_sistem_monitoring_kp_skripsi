import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboradComponent } from './dashborad/dashborad.component';
import { KerjapraktekComponent } from '../layout/kerjapraktek/kerjapraktek.component';
import { SkripsiComponent } from '../layout/skripsi/skripsi.component';
import { DashboarduserComponent } from '../layout/dashboarduser/dashboarduser.component';

const routes: Routes = [
  {
    path: 'dashboard', component: DashboradComponent,
    children: [
      {
        path: 'kerjapraktek',  loadChildren: '../layout/kerjapraktek/kerjapraktek.module#KerjapraktekModule',
      },
      {
        path: 'skripsi',  loadChildren: '../layout/skripsi/skripsi.module#SkripsiModule',
      },
      {
        path: 'dashboarduser',  loadChildren: '../layout/dashboarduser/dashboarduser.module#DashboardModule',
      }
    ],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
