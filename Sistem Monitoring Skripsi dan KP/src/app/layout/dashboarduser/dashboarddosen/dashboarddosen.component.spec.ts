import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboarddosenComponent } from './dashboarddosen.component';

describe('DashboarddosenComponent', () => {
  let component: DashboarddosenComponent;
  let fixture: ComponentFixture<DashboarddosenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboarddosenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboarddosenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
