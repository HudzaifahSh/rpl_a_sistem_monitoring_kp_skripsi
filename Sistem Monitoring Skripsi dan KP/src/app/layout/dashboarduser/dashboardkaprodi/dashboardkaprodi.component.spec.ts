import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardkaprodiComponent } from './dashboardkaprodi.component';

describe('DashboardkaprodiComponent', () => {
  let component: DashboardkaprodiComponent;
  let fixture: ComponentFixture<DashboardkaprodiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardkaprodiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardkaprodiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
