import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Dashboard_kaprodiService} from '../../../services/dashboard_kaprodi/dashboard_kaprodi.service';

@Component({
  selector: 'app-dashboardkaprodi',
  templateUrl: './dashboardkaprodi.component.html',
  styleUrls: ['./dashboardkaprodi.component.scss']
})
export class DashboardkaprodiComponent implements OnInit {

  constructor(
    private api: Dashboard_kaprodiService
  ) { }

  ngOnInit() {
    this.getData();

  dataSource:any=[];

  getData()
  {
    this.api.mhs().subscribe(data=>{
      this.dataSource=data;
    })
  }

