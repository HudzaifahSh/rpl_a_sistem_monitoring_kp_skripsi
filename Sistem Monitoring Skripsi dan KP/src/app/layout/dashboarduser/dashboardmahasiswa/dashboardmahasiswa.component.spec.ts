import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardmahasiswaComponent } from './dashboardmahasiswa.component';

describe('DashboardmahasiswaComponent', () => {
  let component: DashboardmahasiswaComponent;
  let fixture: ComponentFixture<DashboardmahasiswaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardmahasiswaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardmahasiswaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
