import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboarduserComponent } from './dashboarduser.component';
import { DashboardkaprodiComponent } from './dashboardkaprodi/dashboardkaprodi.component';
import { DashboarddosenComponent } from './dashboarddosen/dashboarddosen.component';
import { DashboardmahasiswaComponent } from './dashboardmahasiswa/dashboardmahasiswa.component';
import { HasilpemantauanmhsComponent } from './hasilpemantauanmhs/hasilpemantauanmhs.component';
<<<<<<< HEAD
import { ProfileComponent } from './profile/profile.component';
=======
import { FormsModule } from '@angular/forms';
>>>>>>> 72c32e6f35d1323d0b53429e4b63d13b051f8acc

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: DashboarduserComponent },
            { path: 'dashboardkaprodi', component: DashboardkaprodiComponent },
            { path: 'dashboarddosen', component: DashboarddosenComponent },
            { path: 'dashboardmahasiswa', component: DashboardmahasiswaComponent },
            { path: 'hasilpemantauanmhs', component: HasilpemantauanmhsComponent },
            { path: 'profile', component: ProfileComponent }
        ]),
        FormsModule
    ],
    declarations: [
        
        DashboarduserComponent,
        DashboardkaprodiComponent,
        DashboarddosenComponent,
        DashboardmahasiswaComponent,
        HasilpemantauanmhsComponent,
        ProfileComponent


    ],
})
export class DashboardModule { }
