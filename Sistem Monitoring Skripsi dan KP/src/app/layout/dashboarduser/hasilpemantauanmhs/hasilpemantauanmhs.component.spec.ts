import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HasilpemantauanmhsComponent } from './hasilpemantauanmhs.component';

describe('HasilpemantauanmhsComponent', () => {
  let component: HasilpemantauanmhsComponent;
  let fixture: ComponentFixture<HasilpemantauanmhsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HasilpemantauanmhsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HasilpemantauanmhsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
