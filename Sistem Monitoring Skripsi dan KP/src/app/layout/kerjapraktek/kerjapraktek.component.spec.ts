import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KerjapraktekComponent } from './kerjapraktek.component';

describe('KerjapraktekComponent', () => {
  let component: KerjapraktekComponent;
  let fixture: ComponentFixture<KerjapraktekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KerjapraktekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KerjapraktekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
