import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KerjapraktekComponent } from './kerjapraktek.component';
import { PendaftarankpComponent } from './pendaftarankp/pendaftarankp.component';
import { PemantauankpComponent } from './pemantauankp/pemantauankp.component';
import { VerifikasikpComponent } from './verifikasikp/verifikasikp.component';
import { SeminarkpComponent } from './seminarkp/seminarkp.component';
import { PendaftaranseminarComponent } from './pendaftaranseminar/pendaftaranseminar.component';
import { PenjadwalanseminarComponent } from './penjadwalanseminar/penjadwalanseminar.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: KerjapraktekComponent },
            { path: 'pendaftarankp', component: PendaftarankpComponent },
            { path: 'pemantauankp', component: PemantauankpComponent },
            { path: 'verifikasikp', component: VerifikasikpComponent },
            { path: 'seminarkp', component: SeminarkpComponent},
            { path: 'pendaftaranseminar', component: PendaftaranseminarComponent },
            { path: 'penjadwalanseminar', component: PenjadwalanseminarComponent }
        ]),
    ],
    declarations: [

        KerjapraktekComponent,
        PendaftarankpComponent,
        PemantauankpComponent,
        VerifikasikpComponent,
        SeminarkpComponent,
        PendaftaranseminarComponent,
        PenjadwalanseminarComponent


    ],
})
export class KerjapraktekModule { }
