import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PemantauankpComponent } from './pemantauankp.component';

describe('PemantauankpComponent', () => {
  let component: PemantauankpComponent;
  let fixture: ComponentFixture<PemantauankpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemantauankpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PemantauankpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
