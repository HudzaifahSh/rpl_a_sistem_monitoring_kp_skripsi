import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftarankpComponent } from './pendaftarankp.component';

describe('PendaftarankpComponent', () => {
  let component: PendaftarankpComponent;
  let fixture: ComponentFixture<PendaftarankpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftarankpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftarankpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
