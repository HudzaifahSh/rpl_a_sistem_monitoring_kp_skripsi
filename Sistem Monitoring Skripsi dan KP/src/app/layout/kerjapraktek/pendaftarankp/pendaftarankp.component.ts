import { Component, OnInit } from '@angular/core';
import { ApiService} from '../../../api.service';

@Component({
  selector: 'app-pendaftarankp',
  templateUrl: './pendaftarankp.component.html',
  styleUrls: ['./pendaftarankp.component.scss']
})
export class PendaftarankpComponent implements OnInit {

  constructor(
    private api: ApiService
  ) { }

  ngOnInit() {
    this.api.test_pendaftaran_kp().subscribe(data=>{
      console.log(data);
    },
    error=>{
      console.log(error);
    }
    )
  }

}
