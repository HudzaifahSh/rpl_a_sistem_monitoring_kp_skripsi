import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranseminarComponent } from './pendaftaranseminar.component';

describe('PendaftaranseminarComponent', () => {
  let component: PendaftaranseminarComponent;
  let fixture: ComponentFixture<PendaftaranseminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranseminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranseminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
