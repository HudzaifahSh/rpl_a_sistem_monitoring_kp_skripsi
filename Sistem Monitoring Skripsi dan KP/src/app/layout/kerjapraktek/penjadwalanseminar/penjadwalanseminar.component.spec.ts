import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenjadwalanseminarComponent } from './penjadwalanseminar.component';

describe('PenjadwalanseminarComponent', () => {
  let component: PenjadwalanseminarComponent;
  let fixture: ComponentFixture<PenjadwalanseminarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenjadwalanseminarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenjadwalanseminarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
