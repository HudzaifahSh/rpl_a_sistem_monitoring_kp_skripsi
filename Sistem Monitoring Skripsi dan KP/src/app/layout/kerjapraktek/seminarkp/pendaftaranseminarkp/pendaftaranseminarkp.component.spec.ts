import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranseminarkpComponent } from './pendaftaranseminarkp.component';

describe('PendaftaranseminarkpComponent', () => {
  let component: PendaftaranseminarkpComponent;
  let fixture: ComponentFixture<PendaftaranseminarkpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranseminarkpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranseminarkpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
