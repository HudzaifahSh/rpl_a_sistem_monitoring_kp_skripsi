import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenjadwalanseminarkpComponent } from './penjadwalanseminarkp.component';

describe('PenjadwalanseminarkpComponent', () => {
  let component: PenjadwalanseminarkpComponent;
  let fixture: ComponentFixture<PenjadwalanseminarkpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenjadwalanseminarkpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenjadwalanseminarkpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
