import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SeminarkpComponent } from './seminarkp.component';

describe('SeminarkpComponent', () => {
  let component: SeminarkpComponent;
  let fixture: ComponentFixture<SeminarkpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeminarkpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SeminarkpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
