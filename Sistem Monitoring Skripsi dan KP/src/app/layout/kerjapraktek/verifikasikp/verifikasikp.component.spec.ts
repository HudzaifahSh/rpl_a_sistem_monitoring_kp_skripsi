import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifikasikpComponent } from './verifikasikp.component';

describe('VerifikasikpComponent', () => {
  let component: VerifikasikpComponent;
  let fixture: ComponentFixture<VerifikasikpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifikasikpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifikasikpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
