import { Component, OnInit } from '@angular/core';
import { ApiService} from '../../../api.service';

@Component({
  selector: 'app-verifikasikp',
  templateUrl: './verifikasikp.component.html',
  styleUrls: ['./verifikasikp.component.scss']
})
export class VerifikasikpComponent implements OnInit {

  constructor(
    private api : ApiService
  ) { }

  ngOnInit() {
    this.getVerifikasi();

    // this.api.test_vr().subscribe(data=>{
    //   console.log(data);
    // },
    // error=>{
    //   console.log(error);
    // }
    
    // );
  }

  dataSource:any=[];

  getVerifikasi()
  {
    this.api.get_verifikasi().subscribe(data=>{
      this.dataSource=data;
    });
  }
}
