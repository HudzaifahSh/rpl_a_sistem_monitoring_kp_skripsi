import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PemantauanskripsiComponent } from './pemantauanskripsi.component';

describe('PemantauanskripsiComponent', () => {
  let component: PemantauanskripsiComponent;
  let fixture: ComponentFixture<PemantauanskripsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PemantauanskripsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PemantauanskripsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
