import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaransidangComponent } from './pendaftaransidang.component';

describe('PendaftaransidangComponent', () => {
  let component: PendaftaransidangComponent;
  let fixture: ComponentFixture<PendaftaransidangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaransidangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaransidangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
