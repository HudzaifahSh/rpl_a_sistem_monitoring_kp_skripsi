import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendaftaranskripsiComponent } from './pendaftaranskripsi.component';

describe('PendaftaranskripsiComponent', () => {
  let component: PendaftaranskripsiComponent;
  let fixture: ComponentFixture<PendaftaranskripsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendaftaranskripsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendaftaranskripsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
