import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../api.service';

@Component({
  selector: 'app-pendaftaranskripsi',
  templateUrl: './pendaftaranskripsi.component.html',
  styleUrls: ['./pendaftaranskripsi.component.scss']
})
export class PendaftaranskripsiComponent implements OnInit {

  constructor(
  private api: ApiService
  ) { }

  ngOnInit() {

  this.getData();
   }
 dataSource:any=[];

 getData() {
        this.api.get_pendaftaran_sk().subscribe(data=>{
        this.dataSource=data;
      })
   }
}