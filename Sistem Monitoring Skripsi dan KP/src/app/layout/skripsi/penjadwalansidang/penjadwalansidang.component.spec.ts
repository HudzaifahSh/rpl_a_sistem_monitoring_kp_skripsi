import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PenjadwalansidangComponent } from './penjadwalansidang.component';

describe('PenjadwalansidangComponent', () => {
  let component: PenjadwalansidangComponent;
  let fixture: ComponentFixture<PenjadwalansidangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PenjadwalansidangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PenjadwalansidangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
