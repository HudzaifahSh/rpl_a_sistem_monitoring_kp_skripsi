import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidangskripsiComponent } from './sidangskripsi.component';

describe('SidangskripsiComponent', () => {
  let component: SidangskripsiComponent;
  let fixture: ComponentFixture<SidangskripsiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidangskripsiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidangskripsiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
