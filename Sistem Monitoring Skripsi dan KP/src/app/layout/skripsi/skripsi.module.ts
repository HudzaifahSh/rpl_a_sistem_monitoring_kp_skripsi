import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SkripsiComponent } from './skripsi.component';
import { PendaftaranskripsiComponent } from './pendaftaranskripsi/pendaftaranskripsi.component';
import { PemantauanskripsiComponent } from './pemantauanskripsi/pemantauanskripsi.component';
import { SidangskripsiComponent } from './sidangskripsi/sidangskripsi.component';
import { PenjadwalansidangComponent } from './penjadwalansidang/penjadwalansidang.component';
import { PendaftaransidangComponent } from './pendaftaransidang/pendaftaransidang.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: SkripsiComponent },
            { path: 'pendaftaranskripsi', component: PendaftaranskripsiComponent },
            { path: 'pemantauanskripsi', component: PemantauanskripsiComponent },
            { path: 'sidangskripsi', component: SidangskripsiComponent },
            { path: 'penjadwalansidang', component: PenjadwalansidangComponent },
            { path: 'pendaftaransidang', component: PendaftaransidangComponent },
        ]),
    ],
    declarations: [

        SkripsiComponent,
        PendaftaranskripsiComponent,
        PemantauanskripsiComponent,
        SidangskripsiComponent,
        PenjadwalansidangComponent,
        PendaftaransidangComponent

    ],
})
export class SkripsiModule { }
