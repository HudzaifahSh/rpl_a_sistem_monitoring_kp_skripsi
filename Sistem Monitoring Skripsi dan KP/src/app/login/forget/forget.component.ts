import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $;
@Component({
  selector: 'app-forget',
  templateUrl: './forget.component.html',
  styleUrls: ['./forget.component.scss']
})

export class ForgetComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    document.body.className = 'hold-transition forget-page';
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  }

kembali() {
    this.router.navigate(['login']);
  }

}
