import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService} from '../../services/login/login.service';
declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private api: LoginService
  ) { }

  ngOnInit() {
    document.body.className = 'hold-transition login-page';
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
    this.login.test().subscribe(
      data=>{
      console.log(data);
    },
    error=>{
      console.log(error);
    }
    
    )
  }

  register() {
    this.router.navigate(['register']);
  }
  
  forget() {
    this.router.navigate(['forget']);
  }
  user:any={}
  
  

  // login(user) {
  //   // this.router.navigate(['dashboard']);
  //   this.api.login(user).subscribe(data=>{
  //     console.log(data);
      
  //   this.router.navigate(['dashboard']); 
  //   },
  //   error=>{
  //     console.log(error);
  //   }
    
  //   );
  // }

  }