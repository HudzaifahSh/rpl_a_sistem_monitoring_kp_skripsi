import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class Dashboard_kaprodiService {

  constructor(
    private http:HttpClient
  ) { }

  serverUrl:any='http://localhost/kp_sk/index.php/';
  
  test(){
    return this.http.get(this.serverUrl+'Auth/status');
  }
  mhs()
  {
    return this.http.get(this.serverUrl+'Data_mhs');
  }
}
