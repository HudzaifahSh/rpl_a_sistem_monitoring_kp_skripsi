import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private http:HttpClient
  ) { }

  serverUrl:any='http://localhost/kp_sk/index.php/';
  
  test(){
    return this.http.get(this.serverUrl+'Auth/status');
  }

}
