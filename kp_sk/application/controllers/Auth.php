<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Auth extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');        
        date_default_timezone_set('Asia/Jakarta');
    }
    

    public function login_post()
    {
        $data=$this->post();
        $email = $data["email"];
        $pas = md5($data["password"]);
        $userData = array('email'=>$email,'password'=>$pas);
        $cek = $this->db->get_where("user",$userData)->row();

        $this->response($cek);
    }
    
    function status_get()
    {
        // $data = $this->db->query("SELECT * FROM user");
        $this->response('berhasil',200);
    }

    public function company_post()
    {		 
		  $e=$this->post('email');
		  $p=md5($this->post('password'));
          $name=$this->post('name');
          $address=$this->post('address');
		  $r='company';
		  $cid=uniqid();
		  $userData=array('email'=>$e,'password'=>$p,'role'=>$r,'uid'=>$cid,'confirmed'=>1);
          $companyData=array('cid'=>$cid,'name'=>$name,'address'=>$address);
		  $crt_c = $this->db->insert('company',$companyData);
          $crt_u=$this->db->insert('users_user',$userData);
		  if($crt_c && $crt_u)
		  {
                $q=array('email'=>$e,'password'=>$p);
                $val = $this->db->get_where('users_user',$q)->row();
		  	    $output=$this->createToken($val);
                $this->set_response($output, REST_Controller::HTTP_OK); //This is the respon if success	  
		  }else{
		  		$error=array('status'=>'Tidak dapat menyimpan ke database.');
		  		 $this->set_response($error);
		  }		 
    }
    
   
    function createToken($val)
    {
        date_default_timezone_set('Asia/Jakarta');
        $date = new DateTime();
        $role=$val->role;
    	$kunci =$this->config->item('thekey');
        if($role=='company')
        {
            $cid=$val->uid;
            $usr=$this->db->query("SELECT * FROM company WHERE cid='$cid'")->row();
            $user=array(
                'name'=>$usr->name,           
                'role'=>$role
            );
            $token['uid']=$usr->cid;
            $token['name']=$usr->name;            
        }else if($role=='customer')
        {
             
        }
        $token['role']=$role;        
        $token['email']=$val->email;     
        $token['iat'] = $date->getTimestamp();
        $token['exp'] = $date->getTimestamp() + 60*60*24; //set to 24 hour active
        $output['jwt'] = JWT::encode($token,$kunci ); //This is the output token
        $output['user']=$user;

        return $output;
    }

    
    function reset_post()
    {
        $this->auth();
        $role=$this->user_data->role;
        $un=$this->post('username');
        $email=$this->post('email');
        $newPass=$this->post('newPass');
        $oldPass=$this->post('oldPass');
        if($role=='school')
        {
            if($oldPass!=null)
            {
                $q=$this->db->get_where('users_user',array('username'=>$un,'password'=>md5($oldPass)))->row();
                if($q->username==$un && $newPass!=null)
                {
                    $this->db->where('username',$un);
                    $res=$this->db->update('users_user',array('password'=>md5($newPass)));
                }

            }else{
                $this->db->where('username',$un);
                $res=$this->db->update('users_user',array('password'=>md5($newPass)));
            }
           
        }

        $this->response($res);
    }
    
    
    
    

}
