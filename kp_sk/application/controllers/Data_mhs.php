<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Data_mhs extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_data_mhs');        
        date_default_timezone_set('Asia/Jakarta');
    }
    

    public function index_get()
    {
        $id = $this->get('id');
        $mhs = $this->M_data_mhs->get_mhs($id);

        echo json_encode($mhs);
    }
    
    function status_get()
    {
        // $data = $this->db->query("SELECT * FROM user");
        $this->response('berhasil',200);
    }

}
