<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_data_mhs extends CI_Model{

	function get_mhs($id = null)
	{
		if($id === null)
		{
			return $this->db->get('mahasiswa')->result_array();
		}else{
			return $this->db->get_where('mahasiswa',['id'=> $id])->result_array();
		}

	}
}
