<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_pendaftaran_sk extends CI_Model{

	function get_mhs($id = null)
	{
		if($id === null)
		{
			return $this->db->get('daftar_kp_skripsi')->result_array();
		}else{
			return $this->db->get_where('daftar_kp_skripsi',['id'=> $id])->result_array();
		}

	}
}