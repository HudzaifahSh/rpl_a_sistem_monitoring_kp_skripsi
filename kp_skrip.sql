/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.38-MariaDB : Database - kp_skrip
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kp_skrip` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kp_skrip`;

/*Table structure for table `ayah` */

DROP TABLE IF EXISTS `ayah`;

CREATE TABLE `ayah` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `gaji` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ayah` */

/*Table structure for table `daftar_kp_skripsi` */

DROP TABLE IF EXISTS `daftar_kp_skripsi`;

CREATE TABLE `daftar_kp_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nim` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `prodi` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `keterangan` enum('kp','skripsi') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_kp_skripsi` */

/*Table structure for table `daftar_seminar_kp` */

DROP TABLE IF EXISTS `daftar_seminar_kp`;

CREATE TABLE `daftar_seminar_kp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `judul_kp` varchar(100) NOT NULL,
  `up_ktm` varchar(100) NOT NULL,
  `up_transkip_nilai` varchar(100) NOT NULL,
  `up_kartu_bimbingan` varchar(100) NOT NULL,
  `up_laporan_kp` varchar(100) NOT NULL,
  `up_bukti_pembayaran` varchar(100) NOT NULL,
  `up_krs` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_seminar_kp` */

/*Table structure for table `daftar_sidang_skripsi` */

DROP TABLE IF EXISTS `daftar_sidang_skripsi`;

CREATE TABLE `daftar_sidang_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nim` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `dosen_pembimbing` varchar(100) NOT NULL,
  `upload_file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_sidang_skripsi` */

/*Table structure for table `data_bimbingan` */

DROP TABLE IF EXISTS `data_bimbingan`;

CREATE TABLE `data_bimbingan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tgl_bimbingan` varchar(100) NOT NULL,
  `saran_dosen` text NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_bimbingan` */

/*Table structure for table `data_semester` */

DROP TABLE IF EXISTS `data_semester`;

CREATE TABLE `data_semester` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `kode_makul` varchar(100) NOT NULL,
  `nama_makul` varchar(100) NOT NULL,
  `dosen` varchar(100) NOT NULL,
  `nilai` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_semester` */

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `niy` varchar(100) NOT NULL,
  `nidn` varchar(100) NOT NULL,
  `nama_dos` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

insert  into `dosen`(`id`,`niy`,`nidn`,`nama_dos`,`alamat`,`jabatan`,`status`) values 
(1,'01','01','farid','bantul','dosen','Y');

/*Table structure for table `ibu` */

DROP TABLE IF EXISTS `ibu`;

CREATE TABLE `ibu` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `pekerjaan` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `gaji` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ibu` */

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nim` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `agama` varchar(100) NOT NULL,
  `angkatan` varchar(100) NOT NULL,
  `skripsi` enum('Y','N') NOT NULL,
  `kp` enum('Y','N') NOT NULL,
  `khs` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

insert  into `mahasiswa`(`id`,`nim`,`nama`,`alamat`,`agama`,`angkatan`,`skripsi`,`kp`,`khs`) values 
(2,'123','angga','sekura','islam','2017','Y','Y','ada');

/*Table structure for table `pemantauan_kp_skripsi` */

DROP TABLE IF EXISTS `pemantauan_kp_skripsi`;

CREATE TABLE `pemantauan_kp_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `mahasiswa` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `priode` varchar(100) NOT NULL,
  `upload_file` varchar(100) NOT NULL,
  `keterangan` enum('kp','skripsi') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemantauan_kp_skripsi` */

/*Table structure for table `penjadwalan` */

DROP TABLE IF EXISTS `penjadwalan`;

CREATE TABLE `penjadwalan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nim` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jam` time NOT NULL,
  `tanggal` date NOT NULL,
  `ruang` varchar(100) NOT NULL,
  `penguji` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `penjadwalan` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`email`,`token`) values 
(1,'akbar','202cb962ac59075b964b07152d234b70','akbar','123'),
(2,'akbar','202cb962ac59075b964b07152d234b70','akbar','123'),
(3,'reza','202cb962ac59075b964b07152d234b70','reza','123');

/*Table structure for table `verifikasi_kp` */

DROP TABLE IF EXISTS `verifikasi_kp`;

CREATE TABLE `verifikasi_kp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tahun` varchar(100) NOT NULL,
  `laporan_kp` varchar(100) NOT NULL,
  `tgl_verifikasi` date NOT NULL,
  `dosen_pembimbing` varchar(100) NOT NULL,
  `status` enum('y','n') NOT NULL DEFAULT 'n',
  `nilai` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `verifikasi_kp` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
