/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.1.38-MariaDB : Database - kp_skripsi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`kp_skripsi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `kp_skripsi`;

/*Table structure for table `daftar_kp` */

DROP TABLE IF EXISTS `daftar_kp`;

CREATE TABLE `daftar_kp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `tanggal_pelaksanaan` date NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  KEY `id_mahasiswa` (`id_mahasiswa`),
  CONSTRAINT `daftar_kp_ibfk_1` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_kp` */

/*Table structure for table `daftar_semniar_kp` */

DROP TABLE IF EXISTS `daftar_semniar_kp`;

CREATE TABLE `daftar_semniar_kp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_daftar_kp` int(5) NOT NULL,
  `ktm` varchar(100) NOT NULL,
  `transkip_nilai` varchar(100) NOT NULL,
  `kartu_bimbingan` varchar(100) NOT NULL,
  `laporan_kp` varchar(100) NOT NULL,
  `bukti_pembayaran` varchar(100) NOT NULL,
  `krs` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_daftar_kp` (`id_daftar_kp`),
  CONSTRAINT `daftar_semniar_kp_ibfk_1` FOREIGN KEY (`id_daftar_kp`) REFERENCES `daftar_kp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_semniar_kp` */

/*Table structure for table `daftar_sidang_skrispsi` */

DROP TABLE IF EXISTS `daftar_sidang_skrispsi`;

CREATE TABLE `daftar_sidang_skrispsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_daftar_skripsi` int(5) NOT NULL,
  `laporan_skripsi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_daftar_skripsi` (`id_daftar_skripsi`),
  CONSTRAINT `daftar_sidang_skrispsi_ibfk_1` FOREIGN KEY (`id_daftar_skripsi`) REFERENCES `daftar_skripsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_sidang_skrispsi` */

/*Table structure for table `daftar_skripsi` */

DROP TABLE IF EXISTS `daftar_skripsi`;

CREATE TABLE `daftar_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_mahasiswa` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `instansi` varchar(100) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `tanggal_pelaksanaan` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_mahasiswa` (`id_mahasiswa`),
  CONSTRAINT `daftar_skripsi_ibfk_1` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `daftar_skripsi` */

/*Table structure for table `dosen` */

DROP TABLE IF EXISTS `dosen`;

CREATE TABLE `dosen` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `niy` varchar(100) NOT NULL,
  `nidn` varchar(100) NOT NULL,
  `nama_dos` varchar(100) NOT NULL,
  `id_pendidikan` int(5) NOT NULL,
  `alamat` text NOT NULL,
  `id_user` int(5) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `id_jabatan` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_pendidikan` (`id_pendidikan`),
  KEY `id_jabatan` (`id_jabatan`),
  CONSTRAINT `dosen_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dosen_ibfk_4` FOREIGN KEY (`id_pendidikan`) REFERENCES `pendidikan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dosen_ibfk_5` FOREIGN KEY (`id_jabatan`) REFERENCES `jabatan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `dosen` */

/*Table structure for table `hari` */

DROP TABLE IF EXISTS `hari`;

CREATE TABLE `hari` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `hari` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hari` */

/*Table structure for table `jabatan` */

DROP TABLE IF EXISTS `jabatan`;

CREATE TABLE `jabatan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jabatan` */

/*Table structure for table `jam` */

DROP TABLE IF EXISTS `jam`;

CREATE TABLE `jam` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `jam` varchar(100) NOT NULL,
  `jam_mulai` time NOT NULL,
  `jam_selesai` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jam` */

/*Table structure for table `level` */

DROP TABLE IF EXISTS `level`;

CREATE TABLE `level` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `level` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `level` */

insert  into `level`(`id`,`level`) values 
(1,'admin');

/*Table structure for table `mahasiswa` */

DROP TABLE IF EXISTS `mahasiswa`;

CREATE TABLE `mahasiswa` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama_mhs` varchar(100) NOT NULL,
  `nim` varchar(100) NOT NULL,
  `jenis_kelamin` enum('L','P') NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `tahun` varchar(100) NOT NULL,
  `id_user` int(5) NOT NULL,
  `status` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mahasiswa` */

/*Table structure for table `pemantauan_kp` */

DROP TABLE IF EXISTS `pemantauan_kp`;

CREATE TABLE `pemantauan_kp` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_daftar_kp` int(5) NOT NULL,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_daftar_kp` (`id_daftar_kp`),
  CONSTRAINT `pemantauan_kp_ibfk_1` FOREIGN KEY (`id_daftar_kp`) REFERENCES `daftar_kp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemantauan_kp` */

/*Table structure for table `pemantauan_skripsi` */

DROP TABLE IF EXISTS `pemantauan_skripsi`;

CREATE TABLE `pemantauan_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_daftar_skrispsi` int(5) NOT NULL,
  `file` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_daftar_skrispsi` (`id_daftar_skrispsi`),
  CONSTRAINT `pemantauan_skripsi_ibfk_1` FOREIGN KEY (`id_daftar_skrispsi`) REFERENCES `daftar_skripsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemantauan_skripsi` */

/*Table structure for table `pemantauan_studi` */

DROP TABLE IF EXISTS `pemantauan_studi`;

CREATE TABLE `pemantauan_studi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_dosen` int(5) NOT NULL,
  `id_mahasiswa` int(5) DEFAULT NULL,
  `id_semester` int(5) NOT NULL,
  `materi` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `saran` varchar(100) NOT NULL,
  `tanda_tangan` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`),
  KEY `id_semester` (`id_semester`),
  KEY `id_dosen` (`id_dosen`),
  KEY `id_mahasiswa` (`id_mahasiswa`),
  CONSTRAINT `pemantauan_studi_ibfk_2` FOREIGN KEY (`id_semester`) REFERENCES `semester` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pemantauan_studi_ibfk_3` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pemantauan_studi_ibfk_4` FOREIGN KEY (`id_mahasiswa`) REFERENCES `mahasiswa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemantauan_studi` */

/*Table structure for table `pendidikan` */

DROP TABLE IF EXISTS `pendidikan`;

CREATE TABLE `pendidikan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `pendidikan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pendidikan` */

/*Table structure for table `penjadwalan` */

DROP TABLE IF EXISTS `penjadwalan`;

CREATE TABLE `penjadwalan` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_jam` int(5) NOT NULL,
  `id_hari` int(5) NOT NULL,
  `id_ruang` int(5) NOT NULL,
  `id_rekap_kp_skripsi` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_jam` (`id_jam`),
  KEY `id_hari` (`id_hari`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_rekap_kp_skripsi` (`id_rekap_kp_skripsi`),
  CONSTRAINT `penjadwalan_ibfk_1` FOREIGN KEY (`id_jam`) REFERENCES `jam` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `penjadwalan_ibfk_2` FOREIGN KEY (`id_hari`) REFERENCES `hari` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `penjadwalan_ibfk_3` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `penjadwalan_ibfk_4` FOREIGN KEY (`id_rekap_kp_skripsi`) REFERENCES `rekap_kp_skripsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `penjadwalan` */

/*Table structure for table `rekap_jadwal` */

DROP TABLE IF EXISTS `rekap_jadwal`;

CREATE TABLE `rekap_jadwal` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_dosen` int(5) NOT NULL,
  `id_rekap_kp_skripsi` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_dosen` (`id_dosen`),
  KEY `id_rekap_kp_skripsi` (`id_rekap_kp_skripsi`),
  CONSTRAINT `rekap_jadwal_ibfk_1` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rekap_jadwal_ibfk_2` FOREIGN KEY (`id_rekap_kp_skripsi`) REFERENCES `rekap_kp_skripsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rekap_jadwal` */

/*Table structure for table `rekap_kp_skripsi` */

DROP TABLE IF EXISTS `rekap_kp_skripsi`;

CREATE TABLE `rekap_kp_skripsi` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `id_daftar_seminar_kp` int(5) NOT NULL,
  `id_daftar_sidang_skripsi` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_daftar_seminar_kp` (`id_daftar_seminar_kp`),
  KEY `id_daftar_sidang_skripsi` (`id_daftar_sidang_skripsi`),
  CONSTRAINT `rekap_kp_skripsi_ibfk_1` FOREIGN KEY (`id_daftar_seminar_kp`) REFERENCES `daftar_semniar_kp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rekap_kp_skripsi_ibfk_2` FOREIGN KEY (`id_daftar_sidang_skripsi`) REFERENCES `daftar_sidang_skrispsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `rekap_kp_skripsi` */

/*Table structure for table `ruang` */

DROP TABLE IF EXISTS `ruang`;

CREATE TABLE `ruang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ruang` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ruang` */

/*Table structure for table `semester` */

DROP TABLE IF EXISTS `semester`;

CREATE TABLE `semester` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `semester` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `semester` */

/*Table structure for table `tahun` */

DROP TABLE IF EXISTS `tahun`;

CREATE TABLE `tahun` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `tahun_ajaran` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tahun` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(12) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_level` int(5) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'Y',
  `token` varchar(10) NOT NULL,
  `blocked` varchar(100) NOT NULL,
  `confirmed` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`id_level`,`email`,`no_hp`,`status`,`token`,`blocked`,`confirmed`) values 
(3,'angga1','202cb962ac59075b964b07152d234b70',1,'angga1','123','Y','123','n','y');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
